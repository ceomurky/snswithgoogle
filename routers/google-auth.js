module.exports = function(passport,app){
    const authConfig = require('../config/auth');
    const GoogleStrategy = require('passport-google-oauth20').Strategy;
    const mongoose = require('mongoose');
    var userDb = require('../model/userDb');

    passport.serializeUser(function(user,done){
        done(null,user.userid);
    });
    
    passport.deserializeUser(function(user,done){
        userDb.findOne({userid:user},function(err,getDb){
            done(null,getDb);
        })
    })
    passport.use(new GoogleStrategy(
        authConfig.google,
        function(accessToken, refreshToken, profile, done) {
            userDb.findOne({userid:profile.id},function(err,getDb){
                if(!getDb){
                    var user = new userDb();
                    user.userid = profile.id;
                    user.img = profile._json.image.url;
                    user.save(function(err,docs){
                        return done(null, getDb);
                    })
                }else{
                    return done(null,getDb);
                }
            })
        }
    ));

    app.get('/auth/google',passport.authenticate('google',{scope:['openid email profile']}));
    app.get('/auth/google/callback',passport.authenticate('google',{failureRedirect:'/'}),
    function(req,res){
        if(!req.user.display){
            res.redirect('/profile');
        }
        else{
            res.redirect('/');
        }
    })
}



