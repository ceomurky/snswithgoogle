module.exports = function(app,chekAuth){
    var userDb = require('../model/userDb');
    var postDb = require('../model/postDb');

    const multer = require('multer');
    const path = require('path');
    const resizeImg = require('resize-img');
    const moment = require('moment');


    const upload = multer({
        storage : multer.diskStorage({
            destination : function(req,file,cb){
                cb(null,'public/postimg');
            },
            filename: function(req,file,cb){
                cb(null,new Date().valueOf() + path.extname(file.originalname));
            }
        })
    });

    const profileUplload = multer({
        storage : multer.diskStorage({
            destination : function(req,file,cb){
                cb(null,'public/profileimg');
            },
            filename : function(req,file,cb){
                cb(null,new Date().valueOf() + path.extname(file.originalname));
            }
        })
    })

    app.get('/',function(req,res){
        postDb.find().sort({date:-1}).limit(5).exec(function(err,docs){
            if(req.user){
                res.render('main',{
                    user : req.user,
                    posts:docs,
                    moment : moment
                });
            }else{
                res.render('main',{
                    user : false,
                    posts:docs,
                    moment : moment
                });
            }
        })

    })

    app.post('/main/post',upload.single('img'),function(req,res){ //upload just post
        if(req.user.display){
            if(req.file){
                var newpost = new postDb();
                newpost.display = req.user.display;
                newpost.profileimg = req.user.img;
                newpost.post = req.body.post;
                newpost.img = '/postimg/'+req.file.filename;
                newpost.save(function(err,doc){
                    if(doc)res.redirect('/');
                    else{
                        res.send(`
                        <title> error </title>
                        <script>
                            alert('Failed post upload');
                            location.href='/';
                        </script>
                        `);                    
                    }
                })
            }
            else if(req.body.youtube.length > 10){
                YouId(req.body.youtube,(back)=>{
                    if(back){
                        var newpost = new postDb();
                        newpost.display = req.user.display;
                        newpost.profileimg = req.user.img;
                        newpost.post = req.body.post;
                        newpost.youtube = back[1];
                        newpost.save(function(err,doc){
                            if(doc) res.redirect('/');
                            else{
                                
                                res.send(`
                                <title> error </title>
                                <script>
                                    alert('Failed post upload');
                                    location.href='/';
                                </script>
                                `);                    
                            }
                        })
                    }else{
                        res.send(`
                        <title> error </title>
                        <script>
                            alert('Invalid YouTube address');
                            location.href='/';
                        </script>
                        `);     
                    }
                })
            }else{
                var newpost = new postDb();
                newpost.display = req.user.display;
                newpost.profileimg = req.user.img;
                newpost.post = req.body.post;
                newpost.save(function(err,doc){
                    if(doc) res.redirect('/');
                    else{
                        res.send(`
                        <title> error </title>
                        <script>
                            alert('Failed post upload');
                            location.href='/';
                        </script>
                        `); 
                    }
                })
            }
        }else{
            res.send(`
            <title> Welcome - Error </title>
            <script>
                alert('Please be login');
                location.href='/';
            </script>
            `); 
        }
    });
    app.post('/main/more',function(req,res){
        postDb.find().sort({date:-1}).skip( parseInt(req.body.skip) ).limit(5).exec(function(err,getDb){
            var user = req.user ? req.user : false;
            if(err) throw err;
            else{
                if(getDb){
                    res.send({result:true,dbs : getDb,
                        user:user
                    });
                }else{
                    res.send({result:false,note : 'Db error'})                    
                }
            }

        })
    })
    app.post('/main/reply',function(req,res){
        if(req.user){
            postDb.findById(req.body.code,function(err,getDb){
                getDb.reply.push({
                    display : req.user.display,
                    comment : req.body.comment
                })
                getDb.save(function(err,doc){
                    res.send({result:true,display:req.user.display,count:doc.reply.length});
                    
                })
            })
        }else{
            res.send({result:false,note:'Please Login'});
        }

    })
    function YouId(url,back){
        if(url){
            var regExp = /^.*(?:youtu\.?be(?:\.com)?\/)(?:embed\/)?(?:(?:(?:(?:watch\?)?(?:time_continue=(?:[0-9]+))?.+v=)?([a-zA-Z0-9_-]+))(?:\?t\=(?:[0-9a-zA-Z]+))?)/;
            var matchs = url.match(regExp);
            back(matchs);

        }
    }
    app.get('/profile',chekAuth,function(req,res){
        res.render('profile',{
            user : req.user,
        });
    })

    const fs = require('fs');
    app.post('/profile/upload',profileUplload.single('img'),function(req,res){
        if(req.file){
            fs.exists('./public'+req.user.img,function(exec){
                if(exec){
                    fs.unlink('./public'+req.user.img,function(err){
                        userDb.findOne({userid:req.user.userid},function(err,getDb){
                            getDb.img = '/profileimg/'+req.file.filename;
                            getDb.save(function(err,doc){
                                res.send({result:true,path:'/profileimg/'+req.file.filename,note:"Success upload image"})
                            });
                        })
                    })
                }else{
                    userDb.findOne({userid:req.user.userid},function(err,getDb){
                        getDb.img = '/profileimg/'+req.file.filename;
                        getDb.save(function(err,doc){
                            res.send({result:true,path:'/profileimg/'+req.file.filename,note:"Success upload image"})
                        });
                    })
                }
            })
            
        }else{
            res.send({result:false,note:"Failed upload image"});
        }
    })

    app.post('/profile/nickname',function(req,res){
        userDb.findOne({userid:req.user.userid},function(err,getDb){
            if(getDb){
                userDb.findOne({display:req.body.display},function(err,doc){
                    if(doc){
                        res.send({result:false,note:"Already in use"});        
                    }else{
                        getDb.display = req.body.display;
                        getDb.save(function(err,getDbsave){
                            if(getDbsave){
                                res.send({result:true,note:"Success change name"});
                            }
                        })
                    }
                })                
            }else{
                res.send({result:false,note:"Failed Change name"});
            }
        })
    })
}