const express = require('express');
const session = require('express-session');

const bodyParser = require('body-parser');
const mongoose  = require('mongoose');


mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/sns');

const db = mongoose.connection;
db.on('error',console.error);
db.once('open',function(){
    console.log('Mongodb connect');
})

const app = express();
const passport = require('passport');

const http = require('http').Server(app);


app.use(session({
    secret: '%#$F^ADK^&&)A4jA31',
    resave: false,
    saveUninitialized: false
}))
app.use(passport.initialize());
app.use(passport.session());

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({extended:true}));

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.static('public'));
app.engine('html',require('ejs').renderFile);




function checkAuth(req,res,next){
    if(req.isAuthenticated()){
        return next();
    }
    res.redirect('/');
}

app.get('/info',function(req,res){
    res.sendFile(__dirname + '/views/info.html');
})

app.get('/info/kr',function(req,res){
    res.sendFile(__dirname + '/views/info_kr.html');
})


http.listen(80,function(){
    console.log('server started on 80');
})


var main = require('./routers/main.js')(app,checkAuth);
var google_auth = require('./routers/google-auth.js')(passport,app,checkAuth);