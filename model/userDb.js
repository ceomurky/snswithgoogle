var mongoose = require('mongoose');

var Schema = mongoose.Schema;

    var user = new Schema({
        userid : String,
        display : String,
        img :String, //file path
        date:{type:Date,default: Date.now},
        post:[{ pid : String}]
});

module.exports = mongoose.model('user',user);