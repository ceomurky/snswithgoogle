var mongoose = require('mongoose');

var Schema = mongoose.Schema;

    var post = new Schema({
        display : String, //nick
        profileimg :String, //user profile path
        reply:[{display:String,comment:String,date:{type:Date,default:Date.now}}],
        post : String,
        youtube:String,
        img:String,
        date:{type:Date,default: Date.now},
});

module.exports = mongoose.model('post',post);